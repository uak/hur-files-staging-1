Title: Khalid bin Saeed
Category: Translation,Programming
Tags: JavaScript programmer,Russian-Arabic translator,Original language is Arabic,Experience in the Russian language,Programming experience +5 years

# Services of Khalid bin Saeed

## Translation

### Russian-Arabic

Translation of clips from Russian to Arabic Average quality and very little effort.

## Programming

### JavaScript

JavaScript programming as an advanced developer And great experience working on sites with thousands of visitors

### websites

- [personal](khaledsomething.com)
- [Khamsat](https://khamsat.com/user/khaledsomething)
- [github](https://github.com/khaledsomething/)

### Contact Accounts

- [telegram](t.me/khaledsomething)
- [email](khaledsomething@mywebsite.com)