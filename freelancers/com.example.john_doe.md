Title: John Doe
Category: Programming
Tags: Programming,javascript

![Javascript programming](https://randomuser.me/api/portraits/men/32.jpg)

I'm a programmer working on many professional projects. I like to do translations from time to time on interesting topics. I'm a fan of crypto currencies and free market.

Hourly Rate: 50

# Services of John Doe

## Programming - javascript

### Javascript programming

I've few years experience in Javascript. I've worked with next and svelte. I'm paid hourly.

Service price: 50

![Javascript programming](https://i.imgur.com/KL9n15g.png)

### websites

- [https://github.com/alessandro-tucci-visiontech](https://github.com/alessandro-tucci-visiontech)