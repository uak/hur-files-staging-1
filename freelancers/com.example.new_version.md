Title: John Smith
Category: Programming,Design,Design
Tags: Design,C,image,Programming,logo

![Professional logo design](https://randomuser.me/api/portraits/men/78.jpg)

Senior developer worked on large projects with few team members. I enjoy football and seafood

Hourly Rate: 70

# Services of John Smith

## Programming - C

### Professional C development

I can help you build your project in C language for maximum speed and efficiency

Service price: 80

![Professional C development](https://i.imgur.com/IiW33MY.jpeg)

## Design - image

### Design Images for Your Business

I've experience in using Gimp to create professional images for your website and business

Service price: 50

![Design Images for Your Business](https://i.imgur.com/dcn2Z12.png)

## Design - logo

### Professional logo design

I've created several logos for respected business that got approved by higher management and are being used as brand logo

Service price: 60

![Professional logo design](https://i.imgur.com/Qk9mn4G.gif)

### websites

- [https://www.duhig.ca](https://www.duhig.ca)
- [https://www.faw.ca](https://www.faw.ca)