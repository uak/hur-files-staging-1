import os
import sys
import requests
import toml
from pathlib import Path
from datetime import datetime

# Path to the TOML file to store ETag information
TOML_FILE = "file_status.toml"

# Directory to save downloaded files
output_dir = sys.argv[1]


def load_file_status():
    if Path(TOML_FILE).exists():
        return toml.load(TOML_FILE)
    return {}


def save_file_status(file_status):
    with open(TOML_FILE, "w") as f:
        toml.dump(file_status, f)


def get_etag(url):
    try:
        response = requests.head(url)
        response.raise_for_status()
        etag = response.headers.get("ETag")
        return etag, None  # Return ETag and no error
    except requests.RequestException as e:
        return None, str(e)  # Return None for ETag and the error message


def download_file(url, file_path):
    try:
        response = requests.get(url)
        response.raise_for_status()
        with open(file_path, "wb") as file:
            file.write(response.content)
        print(f"Downloaded {url} to {file_path}")
    except requests.RequestException as e:
        print(f"Error downloading {url}: {e}")


def check_files_for_updates(files):
    file_status = load_file_status()

    for url in files:
        etag, error = get_etag(url)
        current_time = datetime.now().isoformat()

        if url not in file_status:
            file_status[url] = {
                "etag": None,
                "last_updated": None,
                "last_checked": current_time,
                "status": "new",
            }

        if etag:
            if etag != file_status[url]["etag"]:
                print(f"{url} has been updated.")
                file_status[url]["etag"] = etag
                file_status[url]["last_updated"] = current_time

                # Download the file if ETag is different
                file_name = Path(url).name
                file_path = Path(output_dir) / file_name
                download_file(url, file_path)
            else:
                print(f"{url} has \033[31mnot\033[0m been updated.")
            file_status[url]["status"] = "success"
        else:
            print(f"Could not retrieve ETag for {url}. Error: {error}")
            file_status[url]["status"] = "error"
            file_status[url]["error_message"] = error

        file_status[url]["last_checked"] = current_time

    save_file_status(file_status)


def load_urls_from_toml(file_path):
    with open(file_path, "r") as file:
        data = toml.load(file)
        urls = data.get("completed_urls", [])
    return urls


def main():
    url_file = os.getenv("URL_FILE")
    if not url_file:
        print("Environment variable URL_FILE is not set.")
        return

    if not Path(url_file).exists():
        print(f"File specified in URL_FILE does not exist: {url_file}")
        return

    files_to_check = load_urls_from_toml(url_file)

    # Create the download directory if it doesn't exist
    Path(output_dir).mkdir(parents=True, exist_ok=True)

    # ~ # Initial check
    # ~ print("Initial check:")
    # ~ check_files_for_updates(files_to_check)

    # Subsequent checks
    print("\nSubsequent check:")
    check_files_for_updates(files_to_check)


if __name__ == "__main__":
    main()
